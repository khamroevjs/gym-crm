package com.khamroev.gymcrm.service.trainee;

import com.khamroev.gymcrm.controller.TraineeController;
import com.khamroev.gymcrm.dao.trainee.TraineeDao;
import com.khamroev.gymcrm.exception.EntityNotFoundException;
import com.khamroev.gymcrm.model.Trainee;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TraineeServiceImpl implements TraineeService {
    private final Logger log = LoggerFactory.getLogger(TraineeServiceImpl.class);
    private final TraineeDao traineeDao;

    @Autowired
    public TraineeServiceImpl(TraineeDao traineeDao) {
        this.traineeDao = traineeDao;
    }

    @Override
    public Trainee save(Trainee trainee) {
        log.info("Saving trainee with usedId {}", trainee.getUserId());
        Trainee saved = traineeDao.save(trainee);
        log.info("Trainee with usedId {} saved with id {}", saved.getUserId(), saved.getId());
        return saved;
    }

    @Override
    public Trainee update(Trainee trainee) {
        log.info("Updating trainee with id {}", trainee.getId());
        if (!traineeDao.existsById(trainee.getId())) {
            log.info("Trainee with id {} doesn't exists", trainee.getId());
            throw new EntityNotFoundException(String.format("Trainee with given %d id doesn't exists", trainee.getId()));
        }
        Trainee updated = traineeDao.update(trainee);
        log.info("Trainee with id {} updated", updated.getId());
        return updated;
    }

    @Override
    public void deleteById(long id) {
        log.info("Deleting trainee with id {}", id);
        if (!traineeDao.deleteById(id)) {
            log.info("Trainee with id {} doesn't exists", id);
            throw new EntityNotFoundException(String.format("Trainee with given %d id doesn't exists", id));
        }
        log.info("Trainee with id {} deleted", id);
    }

    @Override
    public Trainee getById(long id) {
        log.info("Getting trainee with id {}", id);
        Trainee trainee = traineeDao.getById(id);
        if (trainee == null) {
            log.info("Trainee with id {} doesn't exists", id);
            throw new EntityNotFoundException(String.format("Trainee with given %d id doesn't exists", id));
        }
        log.info("Trainee with id {} found", id);
        return trainee;
    }
}
