package com.khamroev.gymcrm.service.trainee;

import com.khamroev.gymcrm.model.Trainee;

public interface TraineeService {
    Trainee save(Trainee trainee);

    Trainee update(Trainee trainee);

    void deleteById(long id);

    Trainee getById(long id);
}
