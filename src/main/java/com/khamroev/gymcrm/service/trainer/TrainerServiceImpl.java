package com.khamroev.gymcrm.service.trainer;

import com.khamroev.gymcrm.controller.TraineeController;
import com.khamroev.gymcrm.dao.trainer.TrainerDao;
import com.khamroev.gymcrm.exception.EntityNotFoundException;
import com.khamroev.gymcrm.model.Trainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TrainerServiceImpl implements TrainerService {
    private final Logger log = LoggerFactory.getLogger(TrainerServiceImpl.class);
    private final TrainerDao trainerDao;

    @Autowired
    public TrainerServiceImpl(TrainerDao trainerDao) {
        this.trainerDao = trainerDao;
    }

    @Override
    public Trainer save(Trainer trainer) {
        log.info("Saving trainer with usedId {}", trainer.getUserId());
        Trainer saved = trainerDao.save(trainer);
        log.info("Trainer with usedId {} saved with id {}", saved.getUserId(), saved.getId());
        return saved;
    }

    @Override
    public Trainer update(Trainer trainer) {
        log.info("Updating trainer with id {}", trainer.getId());
        if (!trainerDao.existsById(trainer.getId())) {
            log.info("Trainer with id {} doesn't exists", trainer.getId());
            throw new EntityNotFoundException(String.format("Trainer with given %d id doesn't exists", trainer.getId()));
        }
        Trainer updated = trainerDao.update(trainer);
        log.info("Trainer with id {} updated", trainer.getId());
        return updated;
    }

    @Override
    public Trainer getById(long id) {
        log.info("Getting trainer with id {}", id);
        Trainer trainer = trainerDao.getById(id);
        if (trainer == null) {
            log.info("Trainer with id {} doesn't exists", id);
            throw new EntityNotFoundException(String.format("Trainer with given %d id doesn't exists", id));
        }
        log.info("Trainer with id {} found", id);
        return trainer;
    }

}
