package com.khamroev.gymcrm.service.trainer;

import com.khamroev.gymcrm.model.Trainer;

public interface TrainerService {
    Trainer save(Trainer trainer);
    Trainer update(Trainer trainer);
    Trainer getById(long id);

}
