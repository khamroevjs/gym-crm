package com.khamroev.gymcrm.service.user;

import com.khamroev.gymcrm.dao.user.UserDao;
import com.khamroev.gymcrm.exception.EntityNotFoundException;
import com.khamroev.gymcrm.exception.ShortPasswordException;
import com.khamroev.gymcrm.exception.UsernameAlreadyTakenException;
import com.khamroev.gymcrm.model.User;
import com.khamroev.gymcrm.service.training.TrainingServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;

@Service
public class UserServiceImpl implements UserService {
    private final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);
    private static final String CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    private static final int MIN_PASSWORD_LENGTH = 10;
    private final UserDao userDao;

    @Autowired
    public UserServiceImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public User save(User user) {
        log.info("Saving user with first name {} and last name {}", user.getFirstName(), user.getLastName());
        String username = generateUsername(user.getFirstName(), user.getLastName());
        String password = generatePassword(MIN_PASSWORD_LENGTH);
        user.setUsername(username);
        user.setPassword(password);
        User saved = userDao.save(user);
        log.info("User with username {} saved with id {}", saved.getUsername(), saved.getId());
        return saved;
    }

    @Override
    public User update(User user) {
        log.info("Updating user with id {}", user.getId());
        if (userDao.countUsername(user.getUsername()) > 0) {
            log.info("Username {} is already taken", user.getUsername());
            throw new UsernameAlreadyTakenException(String.format("Username %s is already taken", user.getUsername()));
        }
        if (user.getPassword().length() < MIN_PASSWORD_LENGTH) {
            log.info("Password must be at least {} characters long", MIN_PASSWORD_LENGTH);
            throw new ShortPasswordException(String.format("Password must be at least %d characters long", MIN_PASSWORD_LENGTH));
        }
        User updated = userDao.update(user);
        log.info("User with id {} updated", user.getId());
        return updated;
    }

    @Override
    public User getById(long id) {
        log.info("Getting user with id {}", id);
        User user = userDao.getById(id);
        if (user == null) {
            log.info("User with id {} not found", id);
            throw new EntityNotFoundException(String.format("User with id %d not found", id));
        }
        log.info("User with id {} found", id);
        return user;
    }

    @Override
    public void deleteById(long id) {
        log.info("Deleting user with id {}", id);
        if (!userDao.deleteById(id)) {
            log.info("User with id {} not found", id);
            throw new EntityNotFoundException(String.format("User with id %d not found", id));
        }
        log.info("User with id {} deleted", id);
    }

    @Override
    public String generateUsername(String firstName, String lastName) {
        String username = firstName + "." + lastName;
        int count = userDao.countUsername(username);
        if (count > 0) {
            username += count;
        }
        return username;
    }

    @Override
    public String generatePassword(int length) {
        SecureRandom random = new SecureRandom();
        StringBuilder password = new StringBuilder();
        for (int i = 0; i < length; i++) {
            int randomIndex = random.nextInt(CHARACTERS.length());
            char randomChar = CHARACTERS.charAt(randomIndex);
            password.append(randomChar);
        }

        return password.toString();
    }
}
