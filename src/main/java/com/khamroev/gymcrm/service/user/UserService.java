package com.khamroev.gymcrm.service.user;

import com.khamroev.gymcrm.model.User;

public interface UserService {
    User save(User user);
    User update(User user);
    User getById(long id);
    void deleteById(long id);
    String generateUsername(String firstName, String lastName);
    String generatePassword(int length);
}
