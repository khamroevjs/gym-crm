package com.khamroev.gymcrm.service.trainingtype;

import com.khamroev.gymcrm.dao.trainingtype.TrainingTypeDao;
import com.khamroev.gymcrm.exception.EntityNotFoundException;
import com.khamroev.gymcrm.model.TrainingType;
import com.khamroev.gymcrm.service.training.TrainingServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TrainingTypeServiceImpl implements TrainingTypeService {
    private final Logger log = LoggerFactory.getLogger(TrainingTypeServiceImpl.class);
    private final TrainingTypeDao trainingTypeDao;

    @Autowired
    public TrainingTypeServiceImpl(TrainingTypeDao trainingTypeDao) {
        this.trainingTypeDao = trainingTypeDao;
    }

    @Override
    public TrainingType save(TrainingType trainingType) {
        log.info("Saving training type with trainingTypeName {}", trainingType.getTrainingTypeName());
        TrainingType saved = trainingTypeDao.save(trainingType);
        log.info("Training type with trainingTypeName {} saved with id {}", saved.getTrainingTypeName(), saved.getId());
        return saved;
    }

    @Override
    public TrainingType getById(long id) {
        log.info("Getting training type with id {}", id);
        TrainingType trainingType = trainingTypeDao.getById(id);
        if (trainingType == null) {
            log.info("Training type with id {} not found", id);
            throw new EntityNotFoundException(String.format("Training type with id %d not found", id));
        }
        log.info("Training type with id {} found", id);
        return trainingType;
    }

    @Override
    public void deleteById(long id) {
        log.info("Deleting training type with id {}", id);
        if (!trainingTypeDao.deleteById(id)) {
            log.info("Training type with id {} not found", id);
            throw new EntityNotFoundException(String.format("Training type with id %d not found", id));
        }
        log.info("Training type with id {} deleted", id);
    }
}
