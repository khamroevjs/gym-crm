package com.khamroev.gymcrm.service.trainingtype;

import com.khamroev.gymcrm.model.TrainingType;

public interface TrainingTypeService {

    TrainingType save(TrainingType trainingType);
    TrainingType getById(long id);
    void deleteById(long id);
}
