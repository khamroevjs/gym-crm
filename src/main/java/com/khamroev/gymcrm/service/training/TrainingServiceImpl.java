package com.khamroev.gymcrm.service.training;

import com.khamroev.gymcrm.dao.training.TrainingDao;
import com.khamroev.gymcrm.exception.EntityNotFoundException;
import com.khamroev.gymcrm.model.Training;
import com.khamroev.gymcrm.service.trainer.TrainerServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TrainingServiceImpl implements TrainingService {
    private final Logger log = LoggerFactory.getLogger(TrainingServiceImpl.class);
    private final TrainingDao trainingDao;

    @Autowired
    public TrainingServiceImpl(TrainingDao trainingDao) {
        this.trainingDao = trainingDao;
    }

    @Override
    public Training save(Training training) {
        log.info("Saving training with trainingName {}", training.getTrainingName());
        Training saved = trainingDao.save(training);
        log.info("Training with trainingName {} saved with id {}", saved.getTrainingName(), saved.getId());
        return saved;
    }

    @Override
    public Training getById(long id) {
        log.info("Getting training with id {}", id);
        Training training = trainingDao.getById(id);
        if (training == null) {
            log.info("Training with id {} doesn't exists", id);
            throw new EntityNotFoundException(String.format("Training with given %d id doesn't exists", id));
        }
        log.info("Training with id {} found", id);
        return training;
    }
}
