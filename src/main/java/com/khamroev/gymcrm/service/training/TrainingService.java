package com.khamroev.gymcrm.service.training;

import com.khamroev.gymcrm.model.Training;

public interface TrainingService {
    Training save(Training training);

    Training getById(long id);
}
