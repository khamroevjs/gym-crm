package com.khamroev.gymcrm.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class ShortPasswordException extends RuntimeException {
    public ShortPasswordException(String message) {
        super(message);
    }
}
