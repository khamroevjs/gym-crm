package com.khamroev.gymcrm.controller;

import com.khamroev.gymcrm.model.Trainer;
import com.khamroev.gymcrm.model.TrainingType;
import com.khamroev.gymcrm.service.trainingtype.TrainingTypeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/training-types")
public class TrainingTypeController {
    private final Logger log = LoggerFactory.getLogger(TrainingTypeController.class);

    private final TrainingTypeService trainingTypeService;

    @Autowired
    public TrainingTypeController(TrainingTypeService trainingTypeService) {
        this.trainingTypeService = trainingTypeService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<TrainingType> getTrainingTypeById(@PathVariable("id") long id) {
        log.info("Getting training type with id {}", id);
        var response = ResponseEntity.ok(trainingTypeService.getById(id));
        log.info("Training type with id {} found", id);
        return response;
    }
}
