package com.khamroev.gymcrm.controller;

import com.khamroev.gymcrm.model.Trainee;
import com.khamroev.gymcrm.service.trainee.TraineeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/trainees")
public class TraineeController {
    private final Logger log = LoggerFactory.getLogger(TraineeController.class);
    private final TraineeService traineeService;

    @Autowired
    public TraineeController(TraineeService traineeService) {
        this.traineeService = traineeService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<Trainee> getTraineeById(@PathVariable("id") long id) {
        log.info("Getting trainee with id {}", id);
        var response = ResponseEntity.ok(traineeService.getById(id));
        log.info("Trainee with id {} found", id);
        return response;
    }
}
