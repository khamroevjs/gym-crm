package com.khamroev.gymcrm.controller;

import com.khamroev.gymcrm.model.Trainer;
import com.khamroev.gymcrm.service.trainer.TrainerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/trainers")
public class TrainerController {
    private final Logger log = LoggerFactory.getLogger(TrainerController.class);

    private final TrainerService trainerService;

    @Autowired
    public TrainerController(TrainerService trainerService) {
        this.trainerService = trainerService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<Trainer> getTrainerById(@PathVariable("id") long id) {
        log.info("Getting trainer with id {}", id);
        var response = ResponseEntity.ok(trainerService.getById(id));
        log.info("Trainer with id {} found", id);
        return response;
    }
}
