package com.khamroev.gymcrm.controller;


import com.khamroev.gymcrm.model.Training;
import com.khamroev.gymcrm.model.User;
import com.khamroev.gymcrm.service.training.TrainingService;
import com.khamroev.gymcrm.service.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/users")
public class UserController {
    private final Logger log = LoggerFactory.getLogger(UserController.class);
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<User> getUserById(@PathVariable("id") long id) {
        log.info("Getting user with id {}", id);
        var response = ResponseEntity.ok(userService.getById(id));
        log.info("User with id {} found", id);
        return response;
    }
}
