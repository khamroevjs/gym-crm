package com.khamroev.gymcrm.controller;

import com.khamroev.gymcrm.model.Training;
import com.khamroev.gymcrm.service.training.TrainingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/trainings")
public class TrainingController {

    private final Logger log = LoggerFactory.getLogger(TrainingController.class);

    private final TrainingService trainingService;

    @Autowired
    public TrainingController(TrainingService trainingService) {
        this.trainingService = trainingService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<Training> getTrainingById(@PathVariable("id") long id) {
        log.info("Getting training with id {}", id);
        var response = ResponseEntity.ok(trainingService.getById(id));
        log.info("Training with id {} found", id);
        return response;
    }
}
