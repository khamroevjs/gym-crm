package com.khamroev.gymcrm.model;

import java.time.LocalDate;

public class Training {
    private long id;
    private long traineeId;
    private long trainerId;
    private String trainingName;
    private long trainingTypeId;
    private LocalDate trainingDate;
    private int trainingDuration;

    public Training() {
    }

    public Training(long traineeId, long trainerId, String trainingName, long trainingTypeId, LocalDate trainingDate, int trainingDuration) {
        this.traineeId = traineeId;
        this.trainerId = trainerId;
        this.trainingName = trainingName;
        this.trainingTypeId = trainingTypeId;
        this.trainingDate = trainingDate;
        this.trainingDuration = trainingDuration;
    }

    public Training(long id, long traineeId, long trainerId, String trainingName, long trainingTypeId, LocalDate trainingDate, int trainingDuration) {
        this.id = id;
        this.traineeId = traineeId;
        this.trainerId = trainerId;
        this.trainingName = trainingName;
        this.trainingTypeId = trainingTypeId;
        this.trainingDate = trainingDate;
        this.trainingDuration = trainingDuration;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTraineeId() {
        return traineeId;
    }

    public void setTraineeId(long traineeId) {
        this.traineeId = traineeId;
    }

    public long getTrainerId() {
        return trainerId;
    }

    public void setTrainerId(long trainerId) {
        this.trainerId = trainerId;
    }

    public String getTrainingName() {
        return trainingName;
    }

    public void setTrainingName(String trainingName) {
        this.trainingName = trainingName;
    }

    public long getTrainingTypeId() {
        return trainingTypeId;
    }

    public void setTrainingTypeId(long trainingTypeId) {
        this.trainingTypeId = trainingTypeId;
    }

    public LocalDate getTrainingDate() {
        return trainingDate;
    }

    public void setTrainingDate(LocalDate trainingDate) {
        this.trainingDate = trainingDate;
    }

    public int getTrainingDuration() {
        return trainingDuration;
    }

    public void setTrainingDuration(int trainingDuration) {
        this.trainingDuration = trainingDuration;
    }
}
