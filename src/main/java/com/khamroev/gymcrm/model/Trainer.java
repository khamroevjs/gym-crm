package com.khamroev.gymcrm.model;

public class Trainer {
    private long id;
    private long specialization;
    private long userId;

    public Trainer() {
    }

    public Trainer(long specialization, long userId) {
        this.specialization = specialization;
        this.userId = userId;
    }

    public Trainer(long id, long specialization, long userId) {
        this.id = id;
        this.specialization = specialization;
        this.userId = userId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getSpecialization() {
        return specialization;
    }

    public void setSpecialization(long specialization) {
        this.specialization = specialization;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }
}
