package com.khamroev.gymcrm.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.khamroev.gymcrm.dao.trainee.TraineeDao;
import com.khamroev.gymcrm.dao.trainer.TrainerDao;
import com.khamroev.gymcrm.dao.training.TrainingDao;
import com.khamroev.gymcrm.dao.trainingtype.TrainingTypeDao;
import com.khamroev.gymcrm.dao.user.UserDao;
import com.khamroev.gymcrm.model.*;
import jakarta.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Arrays;

@Configuration
@EnableWebMvc
@PropertySource("classpath:application.properties")
@ComponentScan(basePackages = "com.khamroev.gymcrm")
public class AppConfig {

    private final Logger log = LoggerFactory.getLogger(LocalDate.class);

    @Value("${data.file.trainee}")
    private String traineeFilePath;

    @Value("${data.file.trainer}")
    private String trainerFilePath;

    @Value("${data.file.training}")
    private String trainingFilePath;

    @Value("${data.file.training-type}")
    private String trainingTypeFilePath;

    @Value("${data.file.user}")
    private String userFilePath;

    private final TraineeDao traineeDao;
    private final TrainerDao trainerDao;
    private final TrainingDao trainingDao;
    private final TrainingTypeDao trainingTypeDao;
    private final UserDao userDao;

    @Autowired
    public AppConfig(TraineeDao traineeDao, TrainerDao trainerDao, TrainingDao trainingDao, TrainingTypeDao trainingTypeDao, UserDao userDao) {
        this.traineeDao = traineeDao;
        this.trainerDao = trainerDao;
        this.trainingDao = trainingDao;
        this.trainingTypeDao = trainingTypeDao;
        this.userDao = userDao;
    }

    @PostConstruct
    public void initializeDataFromFile() {
        ObjectMapper mapper = new ObjectMapper()
                .registerModule(new JavaTimeModule());

        try {
            File jsonFile = new File(traineeFilePath);
            Trainee[] trainees = mapper.readValue(jsonFile, Trainee[].class);
            Arrays.stream(trainees).forEach(traineeDao::save);
        } catch (IOException e) {
            log.error("Error while reading trainees from file", e);
        }

        try {
            File jsonFile = new File(trainerFilePath);
            Trainer[] trainers = mapper.readValue(jsonFile, Trainer[].class);
            Arrays.stream(trainers).forEach(trainerDao::save);
        } catch (IOException e) {
            log.error("Error while reading trainers from file", e);
        }

        try {
            File jsonFile = new File(trainingFilePath);
            Training[] trainings = mapper.readValue(jsonFile, Training[].class);
            Arrays.stream(trainings).forEach(trainingDao::save);
        } catch (IOException e) {
            log.error("Error while reading trainings from file", e);
        }

        try {
            File jsonFile = new File(trainingTypeFilePath);
            TrainingType[] trainingTypes = mapper.readValue(jsonFile, TrainingType[].class);
            Arrays.stream(trainingTypes).forEach(trainingTypeDao::save);
        } catch (IOException e) {
            log.error("Error while reading training types from file", e);
        }

        try {
            File jsonFile = new File(userFilePath);
            User[] users = mapper.readValue(jsonFile, User[].class);
            Arrays.stream(users).forEach(userDao::save);
        } catch (IOException e) {
            log.error("Error while reading users from file", e);
        }
    }
}

