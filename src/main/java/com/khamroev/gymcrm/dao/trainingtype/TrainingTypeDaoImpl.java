package com.khamroev.gymcrm.dao.trainingtype;

import com.khamroev.gymcrm.dao.training.TrainingDaoImpl;
import com.khamroev.gymcrm.model.TrainingType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class TrainingTypeDaoImpl implements TrainingTypeDao {
    private final Logger log = LoggerFactory.getLogger(TrainingTypeDaoImpl.class);
    private final Map<Long, TrainingType> trainingTypes = new HashMap<>();
    private long id = 1;

    @Override
    public TrainingType save(TrainingType trainingType) {
        log.debug("Saving training type with id {}", id);
        trainingType.setId(id);
        trainingTypes.put(id, trainingType);
        log.debug("Training type with id {} saved", id);
        id++;
        return trainingType;
    }

    @Override
    public TrainingType getById(long id) {
        log.debug("Getting training type with id {}", id);
        TrainingType trainingType = trainingTypes.get(id);
        log.debug("Training type with id {} found", id);
        return trainingType;
    }

    @Override
    public boolean deleteById(long id) {
        log.debug("Deleting training type with id {}", id);
        boolean removed = trainingTypes.remove(id) != null;
        if (!removed) {
            log.debug("Training type with id {} doesn't exists", id);
            return false;
        }
        log.debug("Training type with id {} deleted", id);
        return true;
    }
}
