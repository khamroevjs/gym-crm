package com.khamroev.gymcrm.dao.trainingtype;

import com.khamroev.gymcrm.model.TrainingType;

public interface TrainingTypeDao {
    TrainingType save(TrainingType trainingType);

    TrainingType getById(long id);

    boolean deleteById(long id);
}
