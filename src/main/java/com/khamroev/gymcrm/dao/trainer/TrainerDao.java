package com.khamroev.gymcrm.dao.trainer;

import com.khamroev.gymcrm.model.Trainer;

public interface TrainerDao {
    Trainer save(Trainer trainer);

    Trainer update(Trainer trainer);

    Trainer getById(long id);

    boolean existsById(long id);
}
