package com.khamroev.gymcrm.dao.trainer;

import com.khamroev.gymcrm.model.Trainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class TrainerDaoImpl implements TrainerDao {
    private final Logger log = LoggerFactory.getLogger(TrainerDaoImpl.class);
    private final Map<Long, Trainer> trainers = new HashMap<>();
    private long id = 1;
    @Override
    public Trainer save(Trainer trainer) {
        log.debug("Saving trainee with id {}", id);
        trainer.setId(id);
        trainers.put(id, trainer);
        log.debug("Trainee with id {} saved", id);
        id++;
        return trainer;
    }

    @Override
    public Trainer update(Trainer trainer) {
        log.debug("Updating trainer with id {}", trainer.getId());
        trainers.put(trainer.getId(), trainer);
        log.debug("Trainer with id {} updated", trainer.getId());
        return trainer;
    }

    @Override
    public Trainer getById(long id) {
        log.debug("Getting trainer with id {}", id);
        Trainer trainer = trainers.get(id);
        log.debug("Trainer with id {} found", id);
        return trainer;
    }

    @Override
    public boolean existsById(long id) {
        log.debug("Checking if trainer with id {} exists", id);
        boolean exists = trainers.containsKey(id);
        if (!exists) {
            log.debug("Trainer with id {} doesn't exists", id);
            return false;
        }
        log.debug("Trainer with id {} exists", id);
        return true;
    }
}
