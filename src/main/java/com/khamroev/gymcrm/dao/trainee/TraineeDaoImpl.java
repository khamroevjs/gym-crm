package com.khamroev.gymcrm.dao.trainee;

import com.khamroev.gymcrm.model.Trainee;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class TraineeDaoImpl implements TraineeDao {
    private final Logger log = LoggerFactory.getLogger(TraineeDaoImpl.class);
    private final Map<Long, Trainee> trainees = new HashMap<>();
    private long id = 1;

    @Override
    public Trainee save(Trainee trainee) {
        log.debug("Saving trainee with id {}", id);
        trainee.setId(id);
        trainees.put(id, trainee);
        log.debug("Trainee with id {} saved", id);
        id++;
        return trainee;
    }

    @Override
    public Trainee getById(long id) {
        log.debug("Getting trainee with id {}", id);
        Trainee trainee = trainees.get(id);
        log.debug("Trainee with id {} found", id);
        return trainee;
    }

    @Override
    public Trainee update(Trainee trainee) {
        log.debug("Updating trainee with id {}", trainee.getId());
        trainees.put(trainee.getId(), trainee);
        log.debug("Trainee with id {} updated", trainee.getId());
        return trainee;
    }

    @Override
    public boolean deleteById(long id) {
        log.debug("Deleting trainee with id {}", id);
        boolean removed = trainees.remove(id) != null;
        if (!removed) {
            log.debug("Trainee with id {} doesn't exists", id);
            return false;
        }
        log.debug("Trainee with id {} deleted", id);
        return true;
    }

    @Override
    public boolean existsById(long id) {
        log.debug("Checking if trainee with id {} exists", id);
        boolean exists = trainees.containsKey(id);
        if (!exists) {
            log.debug("Trainee with id {} doesn't exists", id);
            return false;
        }
        log.debug("Trainee with id {} exists", id);
        return true;
    }
}
