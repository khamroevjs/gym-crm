package com.khamroev.gymcrm.dao.trainee;

import com.khamroev.gymcrm.model.Trainee;

public interface TraineeDao {
    Trainee save(Trainee trainee);

    Trainee getById(long id);

    Trainee update(Trainee trainee);

    boolean deleteById(long id);

    boolean existsById(long id);
}
