package com.khamroev.gymcrm.dao.training;

import com.khamroev.gymcrm.model.Training;

public interface TrainingDao {
    Training save(Training training);

    Training getById(long id);

    boolean existsById(long id);
}
