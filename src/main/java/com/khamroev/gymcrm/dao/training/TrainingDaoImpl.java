package com.khamroev.gymcrm.dao.training;

import com.khamroev.gymcrm.model.Training;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class TrainingDaoImpl implements TrainingDao {
    private final Logger log = LoggerFactory.getLogger(TrainingDaoImpl.class);
    private final Map<Long, Training> trainings = new HashMap<>();
    private long id = 1;

    @Override
    public Training save(Training training) {
        log.debug("Saving training with id {}", id);
        training.setId(id);
        trainings.put(id, training);
        log.debug("Training with id {} saved", id);
        id++;
        return training;
    }

    @Override
    public Training getById(long id) {
        log.debug("Getting training with id {}", id);
        Training training = trainings.get(id);
        log.debug("Training with id {} found", id);
        return training;
    }

    @Override
    public boolean existsById(long id) {
        log.debug("Checking if training with id {} exists", id);
        boolean exists = trainings.containsKey(id);
        if (!exists) {
            log.debug("Training with id {} doesn't exists", id);
            return false;
        }
        log.debug("Training with id {} exists", id);
        return true;
    }
}
