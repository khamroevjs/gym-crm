package com.khamroev.gymcrm.dao.user;

import com.khamroev.gymcrm.dao.trainingtype.TrainingTypeDaoImpl;
import com.khamroev.gymcrm.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class UserDaoImpl implements UserDao {
    private final Logger log = LoggerFactory.getLogger(UserDaoImpl.class);
    private final Map<Long, User> users = new HashMap<>();
    private final Map<String, Integer> usernameCounter = new HashMap<>();
    private long id = 1;

    @Override
    public User save(User user) {
        log.debug("Saving user with id {}", id);
        usernameCounter.put(user.getUsername(), usernameCounter.getOrDefault(user.getUsername(), 0) + 1);
        user.setId(id);
        users.put(id, user);
        log.debug("User with id {} saved", id);
        id++;
        return user;
    }

    @Override
    public User update(User user) {
        log.debug("Updating user with id {}", user.getId());
        users.put(user.getId(), user);
        log.debug("User with id {} updated", user.getId());
        return user;
    }

    @Override
    public User getById(long id) {
        log.debug("Getting user with id {}", id);
        User user = users.get(id);
        log.debug("User with id {} found", id);
        return user;
    }

    @Override
    public boolean existsById(long id) {
        log.debug("Checking if user with id {} exists", id);
        boolean exists = users.containsKey(id);
        log.debug("User with id {} exists", id);
        return exists;
    }

    @Override
    public boolean deleteById(long id) {
        log.debug("Deleting user with id {}", id);
        User user = users.get(id);
        if (user == null) {
            log.debug("User with id {} doesn't exists", id);
            return false;
        }
        usernameCounter.put(user.getUsername(), usernameCounter.get(user.getUsername()) - 1);
        if (usernameCounter.get(user.getUsername()) == 0) {
            usernameCounter.remove(user.getUsername());
        }
        users.remove(id);
        log.debug("User with id {} deleted", id);
        return true;
    }

    @Override
    public int countUsername(String username) {
        return usernameCounter.getOrDefault(username, 0);
    }
}
