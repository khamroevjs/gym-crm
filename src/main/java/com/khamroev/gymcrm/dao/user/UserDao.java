package com.khamroev.gymcrm.dao.user;

import com.khamroev.gymcrm.model.User;

public interface UserDao {
    User save(User user);

    User update(User user);

    User getById(long id);

    boolean existsById(long id);

    boolean deleteById(long id);

    int countUsername(String username);
}
