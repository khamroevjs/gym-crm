package com.khamroev.gymcrm.service;

import com.khamroev.gymcrm.dao.training.TrainingDao;
import com.khamroev.gymcrm.exception.EntityNotFoundException;
import com.khamroev.gymcrm.model.Training;
import com.khamroev.gymcrm.service.training.TrainingService;
import com.khamroev.gymcrm.service.training.TrainingServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class TrainingServiceTest {

    @Mock
    private TrainingDao trainingDao;
    private TrainingService underTest;

    @BeforeEach
    public void setUp() {
        underTest = new TrainingServiceImpl(trainingDao);
    }

    @Test
    public void saveTest() {
        // Given
        Training training = new Training(1, 1, 1, "Yoga Session", 1, LocalDate.now(), 1);
        // When
        when(trainingDao.save(training)).thenReturn(training);
        // Then
        Training actual = underTest.save(training);
        assertThat(training.getId()).isEqualTo(actual.getId());
        assertThat(training.getTraineeId()).isEqualTo(actual.getTraineeId());
        assertThat(training.getTrainerId()).isEqualTo(actual.getTrainerId());
        assertThat(training.getTrainingName()).isEqualTo(actual.getTrainingName());
        assertThat(training.getTrainingTypeId()).isEqualTo(actual.getTrainingTypeId());
        assertThat(training.getTrainingDate()).isEqualTo(actual.getTrainingDate());
        assertThat(training.getTrainingDuration()).isEqualTo(actual.getTrainingDuration());
    }

    @Test
    public void getByIdTest() {
        // Given
        Training training = new Training(1, 1, 1, "Yoga Session", 1, LocalDate.now(), 1);
        // When
        when(trainingDao.getById(training.getId())).thenReturn(training);
        // Then
        Training actual = underTest.getById(1);
        assertThat(training.getId()).isEqualTo(actual.getId());
        assertThat(training.getTraineeId()).isEqualTo(actual.getTraineeId());
        assertThat(training.getTrainerId()).isEqualTo(actual.getTrainerId());
        assertThat(training.getTrainingName()).isEqualTo(actual.getTrainingName());
        assertThat(training.getTrainingTypeId()).isEqualTo(actual.getTrainingTypeId());
        assertThat(training.getTrainingDate()).isEqualTo(actual.getTrainingDate());
        assertThat(training.getTrainingDuration()).isEqualTo(actual.getTrainingDuration());
    }

    @Test
    public void getByIdTest_idNotFound() {
        // Given
        Training training = new Training(1, 1, 1, "Yoga Session", 1, LocalDate.now(), 1);
        // When
        when(trainingDao.getById(training.getId())).thenReturn(null);
        // Then
        assertThatThrownBy(() -> underTest.getById(training.getId()))
                .isInstanceOf(EntityNotFoundException.class)
                .hasMessageContaining(String.format("Training with given %d id doesn't exists", training.getId()));
    }
}
