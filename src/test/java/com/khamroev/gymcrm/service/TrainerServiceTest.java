package com.khamroev.gymcrm.service;

import com.khamroev.gymcrm.dao.trainer.TrainerDao;
import com.khamroev.gymcrm.exception.EntityNotFoundException;
import com.khamroev.gymcrm.model.Trainee;
import com.khamroev.gymcrm.model.Trainer;
import com.khamroev.gymcrm.service.trainer.TrainerService;
import com.khamroev.gymcrm.service.trainer.TrainerServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class TrainerServiceTest {
    @Mock
    private TrainerDao trainerDao;

    private TrainerService underTest;

    @BeforeEach
    public void setUp() {
        underTest = new TrainerServiceImpl(trainerDao);
    }

    @Test
    public void saveTest() {
        // Given
        Trainer trainer = new Trainer(1, 1, 1);
        // When
        when(trainerDao.save(trainer)).thenReturn(trainer);
        // Then
        Trainer actual = underTest.save(trainer);
        assertThat(trainer.getId()).isEqualTo(actual.getId());
        assertThat(trainer.getSpecialization()).isEqualTo(actual.getSpecialization());
        assertThat(trainer.getUserId()).isEqualTo(actual.getUserId());
    }

    @Test
    public void updateTest() {
        // Given
        Trainer trainer = new Trainer(1, 1, 1);
        // When
        when(trainerDao.existsById(trainer.getId())).thenReturn(true);
        when(trainerDao.update(trainer)).thenReturn(trainer);
        // Then
        Trainer actual = underTest.update(trainer);
        assertThat(trainer.getId()).isEqualTo(actual.getId());
        assertThat(trainer.getSpecialization()).isEqualTo(actual.getSpecialization());
        assertThat(trainer.getUserId()).isEqualTo(actual.getUserId());
    }

    @Test
    public void updateTest_idNotFound() {
        // Given
        Trainer trainer = new Trainer(1, 1, 1);
        // When
        when(trainerDao.existsById(trainer.getId())).thenReturn(false);
        // Then
        assertThatThrownBy(() -> underTest.update(trainer))
                .isInstanceOf(EntityNotFoundException.class)
                .hasMessageContaining(String.format("Trainer with given %d id doesn't exists", trainer.getId()));
        verify(trainerDao, never()).update(any());
    }

    @Test
    public void getByIdTest() {
        // Given
        Trainer trainer = new Trainer(1, 1, 1);
        // When
        when(trainerDao.getById(trainer.getId())).thenReturn(trainer);
        // Then
        Trainer actual = underTest.getById(trainer.getId());
        assertThat(trainer.getId()).isEqualTo(actual.getId());
        assertThat(trainer.getSpecialization()).isEqualTo(actual.getSpecialization());
        assertThat(trainer.getUserId()).isEqualTo(actual.getUserId());
    }

    @Test
    public void getByIdTest_idNotFound() {
        // Given
        Trainee trainer = new Trainee(1, LocalDate.now(), "Tashkent", 1);
        // When
        when(trainerDao.getById(trainer.getId())).thenReturn(null);
        // Then
        assertThatThrownBy(() -> underTest.getById(trainer.getId()))
                .isInstanceOf(EntityNotFoundException.class)
                .hasMessageContaining(String.format("Trainer with given %d id doesn't exists", trainer.getId()));
    }
}
