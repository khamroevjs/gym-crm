package com.khamroev.gymcrm.service;

import com.khamroev.gymcrm.dao.trainingtype.TrainingTypeDao;
import com.khamroev.gymcrm.exception.EntityNotFoundException;
import com.khamroev.gymcrm.model.TrainingType;
import com.khamroev.gymcrm.service.trainingtype.TrainingTypeService;
import com.khamroev.gymcrm.service.trainingtype.TrainingTypeServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class TrainingTypeServiceTest {
    @Mock
    private TrainingTypeDao trainingDao;

    private TrainingTypeService underTest;

    @BeforeEach
    public void setUp() {
        underTest = new TrainingTypeServiceImpl(trainingDao);
    }

    @Test
    public void saveTest() {
        // Given
        TrainingType trainingType = new TrainingType(1, "Yoga Session");
        // When
        when(trainingDao.save(trainingType)).thenReturn(trainingType);
        // Then
        TrainingType actual = underTest.save(trainingType);
        assertThat(trainingType.getId()).isEqualTo(actual.getId());
        assertThat(trainingType.getTrainingTypeName()).isEqualTo(actual.getTrainingTypeName());
    }

    @Test
    public void getByIdTest() {
        // Given
        TrainingType trainingType = new TrainingType(1, "Yoga Session");
        // When
        when(trainingDao.getById(trainingType.getId())).thenReturn(trainingType);
        // Then
        TrainingType actual = underTest.getById(trainingType.getId());
        assertThat(trainingType.getId()).isEqualTo(actual.getId());
        assertThat(trainingType.getTrainingTypeName()).isEqualTo(actual.getTrainingTypeName());
        assertThat(trainingType.getTrainingTypeName()).isEqualTo(actual.getTrainingTypeName());
    }

    @Test
    public void getById_idNotFoundTest() {
        // Given
        TrainingType trainingType = new TrainingType(1, "Yoga Session");
        // When
        when(trainingDao.getById(trainingType.getId())).thenReturn(null);
        // Then
        assertThatThrownBy(() -> underTest.getById(trainingType.getId()))
                .isInstanceOf(EntityNotFoundException.class)
                .hasMessageContaining(String.format("Training type with id %d not found", trainingType.getId()));
    }

    @Test
    public void deleteByIdTest() {
        // Given
        TrainingType trainingType = new TrainingType(1, "Yoga Session");
        // When
        when(trainingDao.deleteById(trainingType.getId())).thenReturn(true);
        // Then
        underTest.deleteById(trainingType.getId());
        verify(trainingDao).deleteById(trainingType.getId());
    }

    @Test
    public void deleteById_idNotFoundTest() {
        // When
        when(trainingDao.deleteById(anyLong())).thenReturn(false);
        // Then
        assertThatThrownBy(() -> underTest.deleteById(1))
                .isInstanceOf(EntityNotFoundException.class)
                .hasMessageContaining(String.format("Training type with id %d not found", 1));
    }
}
