package com.khamroev.gymcrm.service;


import com.khamroev.gymcrm.dao.user.UserDao;
import com.khamroev.gymcrm.exception.EntityNotFoundException;
import com.khamroev.gymcrm.exception.ShortPasswordException;
import com.khamroev.gymcrm.exception.UsernameAlreadyTakenException;
import com.khamroev.gymcrm.model.User;
import com.khamroev.gymcrm.service.user.UserService;
import com.khamroev.gymcrm.service.user.UserServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {
    @Mock
    private UserDao userDao;
    private UserService underTest;

    @BeforeEach
    public void setUp() {
        underTest = new UserServiceImpl(userDao);
    }

    @Test
    public void saveTest() {
        // Given
        User user = new User();
        user.setFirstName("First");
        user.setLastName("Last");
        user.setUsername("First.Last");
        // When
        when(userDao.save(any())).thenReturn(user);
        underTest.save(user);
        // Then
        var captor = ArgumentCaptor.forClass(User.class);
        verify(userDao).save(captor.capture());
        User capturedUser = captor.getValue();
        assertThat(capturedUser.getFirstName()).isEqualTo("First");
        assertThat(capturedUser.getLastName()).isEqualTo("Last");
        assertThat(capturedUser.getUsername()).isEqualTo("First.Last");
        assertThat(capturedUser.getPassword().length()).isEqualTo(10);
        assertThat(capturedUser.getPassword()).matches("^[a-zA-Z0-9]*$");
    }

    @Test
    public void updateTest() {
        // Given
        User user = new User(1, "First", "Last", "First.Last", "1234567890", true);
        // When
        when(userDao.countUsername(any())).thenReturn(0);
        when(userDao.update(any())).thenReturn(user);
        // Then
        User actual = underTest.update(user);
        assertThat(actual.getId()).isEqualTo(user.getId());
        assertThat(actual.getFirstName()).isEqualTo(user.getFirstName());
        assertThat(actual.getLastName()).isEqualTo(user.getLastName());
        assertThat(actual.getUsername()).isEqualTo(user.getUsername());
        assertThat(actual.getPassword()).isEqualTo(user.getPassword());
        assertThat(actual.isActive()).isEqualTo(user.isActive());
    }


    @Test
    public void updateTest_UsernameTaken() {
        // Given
        User user = new User(1, "First", "Last", "First.Last", "1234567890", true);
        // When
        when(userDao.countUsername(any())).thenReturn(1);
        // Then
        assertThatThrownBy(() -> underTest.update(user))
                .isInstanceOf(UsernameAlreadyTakenException.class)
                .hasMessageContaining(String.format("Username %s is already taken", user.getUsername()));
    }

    @Test
    public void updateTest_shortPassword() {
        // Given
        User user = new User(1, "First", "Last", "First.Last", "123", true);
        // When
        when(userDao.countUsername(any())).thenReturn(0);
        // Then
        assertThatThrownBy(() -> underTest.update(user))
                .isInstanceOf(ShortPasswordException.class)
                .hasMessageContaining("Password must be at least 10 characters long");
    }

    @Test
    public void getByIdTest() {
        // Given
        User user = new User(1, "First", "Last", "First.Last", "1234567890", true);
        // When
        when(userDao.getById(user.getId())).thenReturn(user);
        // Then
        User actual = underTest.getById(user.getId());
        assertThat(actual.getId()).isEqualTo(user.getId());
        assertThat(actual.getFirstName()).isEqualTo(user.getFirstName());
        assertThat(actual.getLastName()).isEqualTo(user.getLastName());
        assertThat(actual.getUsername()).isEqualTo(user.getUsername());
        assertThat(actual.getPassword()).isEqualTo(user.getPassword());
        assertThat(actual.isActive()).isEqualTo(user.isActive());
    }

    @Test
    public void getByIdTest_notFound() {
        // Given
        User user = new User(1, "First", "Last", "First.Last", "1234567890", true);
        // When
        when(userDao.getById(user.getId())).thenReturn(null);
        // Then
        assertThatThrownBy(() -> underTest.getById(user.getId()))
                .isInstanceOf(EntityNotFoundException.class)
                .hasMessageContaining(String.format("User with id %d not found", user.getId()));
    }

    @Test
    public void deleteByIdTest() {
        // Given
        User user = new User(1, "First", "Last", "First.Last", "1234567890", true);
        // When
        when(userDao.deleteById(user.getId())).thenReturn(true);
        // Then
        underTest.deleteById(user.getId());
        verify(userDao).deleteById(user.getId());
    }

    @Test
    public void deleteByIdTest_notFound() {
        // Given
        long id = 1;
        // When
        when(userDao.deleteById(id)).thenReturn(false);
        // Then
        assertThatThrownBy(() -> underTest.deleteById(id))
                .isInstanceOf(EntityNotFoundException.class)
                .hasMessageContaining(String.format("User with id %d not found", 1));
    }

    @Test
    public void generateUsername() {
        String firstName = "First";
        String lastName = "Last";
        when(userDao.countUsername(any())).thenReturn(0);
        String actual = underTest.generateUsername(firstName, lastName);
        assertThat(actual).isEqualTo("First.Last");
    }

    @Test
    public void generateUsername_taken() {
        String firstName = "First";
        String lastName = "Last";
        when(userDao.countUsername(any())).thenReturn(1);
        String actual = underTest.generateUsername(firstName, lastName);
        assertThat(actual).isEqualTo("First.Last1");
    }

    @Test
    public void generatePassword() {
        int length = 10;
        String password = underTest.generatePassword(length);
        assertThat(password.length()).isEqualTo(10);
        // Use a regular expression to check if the string contains only letters and digits
        assertThat(password).matches("^[a-zA-Z0-9]*$");
    }
}