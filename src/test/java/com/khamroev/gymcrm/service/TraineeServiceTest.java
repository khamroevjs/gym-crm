package com.khamroev.gymcrm.service;

import com.khamroev.gymcrm.dao.trainee.TraineeDao;
import com.khamroev.gymcrm.exception.EntityNotFoundException;
import com.khamroev.gymcrm.model.Trainee;
import com.khamroev.gymcrm.service.trainee.TraineeService;
import com.khamroev.gymcrm.service.trainee.TraineeServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class TraineeServiceTest {
    @Mock
    private TraineeDao traineeDao;

    private TraineeService underTest;

    @BeforeEach
    public void setUp() {
        underTest = new TraineeServiceImpl(traineeDao);
    }

    @Test
    public void saveTest() {
        // Given
        Trainee trainee = new Trainee(LocalDate.now(), "Tashkent", 1);
        // When
        when(traineeDao.save(trainee)).thenReturn(trainee);
        // Then
        Trainee actual = underTest.save(trainee);
        assertThat(trainee.getId()).isEqualTo(actual.getId());
        assertThat(trainee.getDateOfBirth()).isEqualTo(actual.getDateOfBirth());
        assertThat(trainee.getAddress()).isEqualTo(actual.getAddress());
        assertThat(trainee.getUserId()).isEqualTo(actual.getUserId());
    }

    @Test
    public void updateTest() {
        // Given
        Trainee trainee = new Trainee(1, LocalDate.now(), "Tashkent", 1);
        // When
        when(traineeDao.existsById(trainee.getId())).thenReturn(true);
        when(traineeDao.update(trainee)).thenReturn(trainee);
        // Then
        Trainee actual = underTest.update(trainee);
        assertThat(trainee.getId()).isEqualTo(actual.getId());
        assertThat(trainee.getDateOfBirth()).isEqualTo(actual.getDateOfBirth());
        assertThat(trainee.getAddress()).isEqualTo(actual.getAddress());
        assertThat(trainee.getUserId()).isEqualTo(actual.getUserId());
    }

    @Test
    public void updateTest_idNotFound() {
        // Given
        Trainee trainee = new Trainee(1, LocalDate.now(), "Tashkent", 1);
        // When
        when(traineeDao.existsById(trainee.getId())).thenReturn(false);
        // Then
        assertThatThrownBy(() -> underTest.update(trainee))
                .isInstanceOf(EntityNotFoundException.class)
                .hasMessageContaining(String.format("Trainee with given %d id doesn't exists", trainee.getId()));
        verify(traineeDao, never()).update(any());
    }

    @Test
    public void deleteByIdTest() {
        // Given
        Trainee trainee = new Trainee(1, LocalDate.now(), "Tashkent", 1);
        // When
        when(traineeDao.deleteById(trainee.getId())).thenReturn(true);
        // Then
        underTest.deleteById(trainee.getId());
        verify(traineeDao).deleteById(trainee.getId());
    }

    @Test
    public void deleteByIdTest_idNotFound() {
        // When
        when(traineeDao.deleteById(1)).thenReturn(false);
        // Then
        assertThatThrownBy(() -> underTest.deleteById(1))
                .isInstanceOf(EntityNotFoundException.class)
                .hasMessageContaining(String.format("Trainee with given %d id doesn't exists", 1));
    }

    @Test
    public void getByIdTest() {
        // Given
        Trainee trainee = new Trainee(1, LocalDate.now(), "Tashkent", 1);
        // When
        when(traineeDao.getById(trainee.getId())).thenReturn(trainee);
        // Then
        Trainee actual = underTest.getById(trainee.getId());
        assertThat(trainee.getId()).isEqualTo(actual.getId());
        assertThat(trainee.getDateOfBirth()).isEqualTo(actual.getDateOfBirth());
        assertThat(trainee.getAddress()).isEqualTo(actual.getAddress());
        assertThat(trainee.getUserId()).isEqualTo(actual.getUserId());
    }


    @Test
    public void getByIdTest_idNotFound() {
        // Given
        Trainee trainee = new Trainee(1, LocalDate.now(), "Tashkent", 1);
        // When
        when(traineeDao.getById(trainee.getId())).thenReturn(null);
        // Then
        assertThatThrownBy(() -> underTest.getById(trainee.getId()))
                .isInstanceOf(EntityNotFoundException.class)
                .hasMessageContaining(String.format("Trainee with given %d id doesn't exists", trainee.getId()));
    }
}
