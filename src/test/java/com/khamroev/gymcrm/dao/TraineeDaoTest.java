package com.khamroev.gymcrm.dao;

import com.khamroev.gymcrm.dao.trainee.TraineeDao;
import com.khamroev.gymcrm.dao.trainee.TraineeDaoImpl;
import com.khamroev.gymcrm.model.Trainee;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

public class TraineeDaoTest {
    private TraineeDao underTest;

    @BeforeEach
    public void setUp() {
        underTest = new TraineeDaoImpl();
    }

    @Test
    public void saveTest() {
        // Given
        Trainee trainee1 = new Trainee(LocalDate.now(), "Tashkent", 1);
        Trainee trainee2 = new Trainee(LocalDate.now(), "Bukhara", 2);
        // When
        Trainee actual1 = underTest.save(trainee1);
        Trainee actual2 = underTest.save(trainee2);
        // Then
        assertThat(actual1.getId()).isEqualTo(1);
        assertThat(actual2.getId()).isEqualTo(2);
    }

    @Test
    public void getByIdTest() {
        // Given
        Trainee trainee = new Trainee(LocalDate.now(), "Tashkent", 1);
        // When
        underTest.save(trainee);
        Trainee actual = underTest.getById(trainee.getId());
        // Then
        assertThat(actual.getId()).isEqualTo(1);
        assertThat(actual.getDateOfBirth()).isEqualTo(trainee.getDateOfBirth());
        assertThat(actual.getAddress()).isEqualTo(trainee.getAddress());
        assertThat(actual.getUserId()).isEqualTo(trainee.getUserId());
    }

    @Test
    public void getByIdTest_notFound() {
        // When
        Trainee actual = underTest.getById(1);
        // Then
        assertThat(actual).isNull();
    }

    @Test
    public void updateTest() {
        // Given
        Trainee trainee = new Trainee(LocalDate.now(), "Tashkent", 1);
        // When
        underTest.save(trainee);
        trainee.setAddress("Bukhara");
        underTest.update(trainee);
        Trainee actual = underTest.getById(trainee.getId());
        // Then
        assertThat(actual.getAddress()).isEqualTo(trainee.getAddress());
        assertThat(actual.getUserId()).isEqualTo(trainee.getUserId());
        assertThat(actual.getDateOfBirth()).isEqualTo(trainee.getDateOfBirth());
        assertThat(actual.getId()).isEqualTo(trainee.getId());
    }

    @Test
    public void deleteById() {
        // Given
        Trainee trainee = new Trainee(LocalDate.now(), "Tashkent", 1);
        // When
        long id = underTest.save(trainee).getId();
        boolean saved = underTest.existsById(id);
        underTest.deleteById(id);
        boolean exists = underTest.existsById(id);
        // Then
        assertThat(saved).isTrue();
        assertThat(exists).isFalse();
    }

    @Test
    public void deleteById_notFound() {
        // When
        boolean isDeleted = underTest.deleteById(1);
        // Then
        assertThat(isDeleted).isFalse();
    }

    @Test
    public void existsByIdTest() {
        // Given
        Trainee trainee = new Trainee(LocalDate.now(), "Tashkent", 1);
        // When
        boolean beforeSave = underTest.existsById(1);
        underTest.save(trainee);
        boolean afterSave = underTest.existsById(1);
        // Then
        assertThat(beforeSave).isFalse();
        assertThat(afterSave).isTrue();
    }
}
