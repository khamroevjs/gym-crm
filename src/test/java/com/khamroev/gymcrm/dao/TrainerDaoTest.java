package com.khamroev.gymcrm.dao;

import com.khamroev.gymcrm.dao.trainer.TrainerDao;
import com.khamroev.gymcrm.dao.trainer.TrainerDaoImpl;
import com.khamroev.gymcrm.model.Trainer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TrainerDaoTest {
    private TrainerDao underTest;

    @BeforeEach
    public void setUp() {
        underTest = new TrainerDaoImpl();
    }

    @Test
    public void saveTest() {
        // Given
        Trainer trainer1 = new Trainer(1, 1);
        Trainer trainer2 = new Trainer(1, 2);
        // When
        Trainer actual1 = underTest.save(trainer1);
        Trainer actual2 = underTest.save(trainer2);
        // Then
        assertThat(actual1.getId()).isEqualTo(1);
        assertThat(actual2.getId()).isEqualTo(2);
    }

    @Test
    public void getByIdTest() {
        // Given
        Trainer trainer = new Trainer(1, 1);
        // When
        underTest.save(trainer);
        Trainer actual = underTest.getById(trainer.getId());
        // Then
        assertThat(actual.getId()).isEqualTo(1);
        assertThat(actual.getSpecialization()).isEqualTo(trainer.getSpecialization());
        assertThat(actual.getUserId()).isEqualTo(trainer.getUserId());
    }

    @Test
    public void getByIdTest_notFound() {
        // When
        Trainer actual = underTest.getById(1);
        // Then
        assertThat(actual).isNull();
    }

    @Test
    public void updateTest() {
        // Given
        Trainer trainer = new Trainer(1, 1);
        // When
        long id = underTest.save(trainer).getId();
        trainer.setSpecialization(2);
        underTest.update(trainer);
        Trainer actual = underTest.getById(id);
        // Then
        assertThat(actual.getId()).isEqualTo(trainer.getId());
        assertThat(actual.getUserId()).isEqualTo(trainer.getUserId());
        assertThat(actual.getSpecialization()).isEqualTo(trainer.getSpecialization());
    }

    @Test
    public void existsByIdTest() {
        // Given
        Trainer trainer = new Trainer(1, 1);
        // When
        boolean beforeSave = underTest.existsById(1);
        underTest.save(trainer);
        boolean afterSave = underTest.existsById(1);
        // Then
        assertThat(beforeSave).isFalse();
        assertThat(afterSave).isTrue();
    }
}
