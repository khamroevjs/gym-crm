package com.khamroev.gymcrm.dao;

import com.khamroev.gymcrm.dao.trainingtype.TrainingTypeDao;
import com.khamroev.gymcrm.dao.trainingtype.TrainingTypeDaoImpl;
import com.khamroev.gymcrm.model.TrainingType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class TrainingTypeTest {
    private TrainingTypeDao underTest;

    @BeforeEach
    public void setUp() {
        underTest = new TrainingTypeDaoImpl();
    }

    @Test
    public void saveTest() {
        // Given
        TrainingType trainingType1 = new TrainingType("Yoga Session");
        TrainingType trainingType2 = new TrainingType("Karate");
        // When
        TrainingType actual1 = underTest.save(trainingType1);
        TrainingType actual2 = underTest.save(trainingType2);
        // Then
        assertThat(actual1.getId()).isEqualTo(1);
        assertThat(actual2.getId()).isEqualTo(2);
        assertThat(actual1.getTrainingTypeName()).isEqualTo(trainingType1.getTrainingTypeName());
        assertThat(actual2.getTrainingTypeName()).isEqualTo(trainingType2.getTrainingTypeName());
    }

    @Test
    public void getByIdTest() {
        // Given
        TrainingType trainingType = new TrainingType("Karate");
        // When
        long id = underTest.save(trainingType).getId();
        TrainingType actual = underTest.getById(id);
        // Then
        assertThat(actual.getId()).isEqualTo(id);
        assertThat(actual.getTrainingTypeName()).isEqualTo(trainingType.getTrainingTypeName());
    }

    @Test
    public void getByIdTest_notFound() {
        // When
        TrainingType actual = underTest.getById(1);
        // Then
        assertThat(actual).isNull();
    }

    @Test
    public void deleteById() {
        // Given
        TrainingType trainingType = new TrainingType("Karate");
        // When
        long id = underTest.save(trainingType).getId();
        TrainingType beforeDelete = underTest.getById(id);
        boolean isDeleted = underTest.deleteById(id);
        TrainingType afterDelete = underTest.getById(id);
        // Then
        assertThat(isDeleted).isTrue();
        assertThat(beforeDelete).isNotNull();
        assertThat(afterDelete).isNull();
    }

    @Test
    public void deleteById_notFound() {
        // When
        boolean isDeleted = underTest.deleteById(1);
        // Then
        assertThat(isDeleted).isFalse();
    }
}
