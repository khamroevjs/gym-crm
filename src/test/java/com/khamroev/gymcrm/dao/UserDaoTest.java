package com.khamroev.gymcrm.dao;

import com.khamroev.gymcrm.dao.user.UserDao;
import com.khamroev.gymcrm.dao.user.UserDaoImpl;
import com.khamroev.gymcrm.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class UserDaoTest {
    private UserDao underTest;

    @BeforeEach
    public void setUp() {
        underTest = new UserDaoImpl();
    }

    @Test
    public void saveTest() {
        // Given
        User user1 = new User("First", "Last", "First.Last", "1234567890", true);
        User user2 = new User("Steve", "Jobs", "Steve.Jobs", "1234567890", true);
        // When
        User actual1 = underTest.save(user1);
        User actual2 = underTest.save(user2);
        // Then
        assertThat(actual1.getId()).isEqualTo(user1.getId());
        assertThat(actual2.getId()).isEqualTo(user2.getId());
        assertThat(actual1.getFirstName()).isEqualTo(user1.getFirstName());
        assertThat(actual2.getFirstName()).isEqualTo(user2.getFirstName());
        assertThat(actual1.getLastName()).isEqualTo(user1.getLastName());
        assertThat(actual2.getLastName()).isEqualTo(user2.getLastName());
        assertThat(actual1.getPassword()).isEqualTo(user1.getPassword());
        assertThat(actual2.getPassword()).isEqualTo(user2.getPassword());
        assertThat(actual1.isActive()).isEqualTo(user1.isActive());
        assertThat(actual2.isActive()).isEqualTo(user2.isActive());
    }

    @Test
    public void updateTest() {
        // Given
        User user = new User("First", "Last", "First.Last", "1234567890", true);
        // When
        underTest.save(user);
        user.setFirstName("Steve");
        underTest.update(user);
        User actual = underTest.getById(user.getId());
        // Then
        assertThat(actual.getFirstName()).isEqualTo(user.getFirstName());
        assertThat(actual.getLastName()).isEqualTo(user.getLastName());
        assertThat(actual.getPassword()).isEqualTo(user.getPassword());
        assertThat(actual.isActive()).isEqualTo(user.isActive());
    }

    @Test
    public void getByIdTest() {
        // Given
        User user = new User("First", "Last", "First.Last", "1234567890", true);
        // When
        long id = underTest.save(user).getId();
        User actual = underTest.getById(id);
        // Then
        assertThat(actual.getId()).isEqualTo(id);
        assertThat(actual.getFirstName()).isEqualTo(user.getFirstName());
        assertThat(actual.getLastName()).isEqualTo(user.getLastName());
        assertThat(actual.getUsername()).isEqualTo(user.getUsername());
        assertThat(actual.getPassword()).isEqualTo(user.getPassword());
        assertThat(actual.isActive()).isEqualTo(user.isActive());
    }

    @Test
    public void getByIdTest_notFound() {
        // When
        User actual = underTest.getById(1);
        // Then
        assertThat(actual).isNull();
    }

    @Test
    public void existsByIdTest() {
        // Given
        User user = new User("First", "Last", "First.Last", "1234567890", true);
        // When
        boolean beforeSave = underTest.existsById(1);
        underTest.save(user);
        boolean afterSave = underTest.existsById(1);
        // Then
        assertThat(beforeSave).isFalse();
        assertThat(afterSave).isTrue();
    }

    @Test
    public void deleteById() {
        // Given
        User user = new User("First", "Last", "First.Last", "1234567890", true);
        // When
        long id = underTest.save(user).getId();
        boolean saved = underTest.existsById(id);
        underTest.deleteById(id);
        boolean exists = underTest.existsById(id);
        // Then
        assertThat(saved).isTrue();
        assertThat(exists).isFalse();
    }

    @Test
    public void deleteById_notFound() {
        // When
        boolean isDeleted = underTest.deleteById(1);
        // Then
        assertThat(isDeleted).isFalse();
    }

    @Test
    public void countUsername() {
        // Given
        User user = new User("First", "Last", "First.Last", "1234567890", true);
        // When
        int beforeSave = underTest.countUsername(user.getUsername());
        underTest.save(user);
        int afterSave = underTest.countUsername(user.getUsername());
        // Then
        assertThat(beforeSave).isEqualTo(0);
        assertThat(afterSave).isEqualTo(1);
    }
}
