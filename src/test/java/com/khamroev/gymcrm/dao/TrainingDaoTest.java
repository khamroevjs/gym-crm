package com.khamroev.gymcrm.dao;

import com.khamroev.gymcrm.dao.training.TrainingDao;
import com.khamroev.gymcrm.dao.training.TrainingDaoImpl;
import com.khamroev.gymcrm.model.Training;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

public class TrainingDaoTest {
    private TrainingDao underTest;

    @BeforeEach
    public void setUp() {
        underTest = new TrainingDaoImpl();
    }

    @Test
    public void saveTest() {
        // Given
        Training training1 = new Training(1, 1, "Yoga Session", 1, LocalDate.now(), 1);
        Training training2 = new Training(1, 1, "Karate", 1, LocalDate.now(), 1);
        // When
        Training actual1 = underTest.save(training1);
        Training actual2 = underTest.save(training2);
        // Then
        assertThat(actual1.getId()).isEqualTo(1);
        assertThat(actual2.getId()).isEqualTo(2);
    }

    @Test
    public void getByIdTest() {
        // Given
        Training training = new Training(1, 1, 1, "Yoga Session", 1, LocalDate.now(), 1);
        // When
        long id = underTest.save(training).getId();
        // Then
        Training actual = underTest.getById(id);
        assertThat(training.getId()).isEqualTo(actual.getId());
        assertThat(training.getTraineeId()).isEqualTo(actual.getTraineeId());
        assertThat(training.getTrainerId()).isEqualTo(actual.getTrainerId());
        assertThat(training.getTrainingName()).isEqualTo(actual.getTrainingName());
        assertThat(training.getTrainingTypeId()).isEqualTo(actual.getTrainingTypeId());
        assertThat(training.getTrainingDate()).isEqualTo(actual.getTrainingDate());
        assertThat(training.getTrainingDuration()).isEqualTo(actual.getTrainingDuration());
    }

    @Test
    public void getByIdTest_notFound() {
        // When
        Training actual = underTest.getById(1);
        // Then
        assertThat(actual).isNull();
    }

    @Test
    public void existsByIdTest() {
        // Given
        Training training = new Training(1, 1, "Yoga Session", 1, LocalDate.now(), 1);
        // When
        boolean beforeSave = underTest.existsById(1);
        long id = underTest.save(training).getId();
        boolean afterSave = underTest.existsById(id);
        // Then
        assertThat(beforeSave).isFalse();
        assertThat(afterSave).isTrue();
    }
}
