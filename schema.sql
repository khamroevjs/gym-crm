CREATE TABLE user_table
(
    id         BIGSERIAL PRIMARY KEY,
    first_name VARCHAR(255) NOT NULL,
    last_name  VARCHAR(255) NOT NULL,
    username   VARCHAR(255) NOT NULL,
    password   VARCHAR(255) NOT NULL,
    is_active  BOOLEAN      NOT NULL
);

CREATE TABLE training_type
(
    id                 BIGSERIAL PRIMARY KEY,
    training_type_name VARCHAR(255) NOT NULL
);

CREATE TABLE trainee
(
    id            BIGSERIAL PRIMARY KEY,
    date_of_birth DATE,
    address       VARCHAR(255),
    user_id       BIGINT REFERENCES user_table (id)
);

CREATE TABLE trainer
(
    id             BIGSERIAL PRIMARY KEY,
    specialization BIGINT REFERENCES training_type (id),
    user_id        BIGINT REFERENCES user_table (id)
);

CREATE TABLE training
(
    id                BIGSERIAL PRIMARY KEY,
    trainee_id        BIGINT REFERENCES trainee (id),
    trainer_id        BIGINT REFERENCES trainer (id),
    training_name     varchar(255) NOT NULL,
    training_type_id  BIGINT REFERENCES training_type (id),
    training_date     DATE         NOT NULL,
    training_duration INT          NOT NULL
);
