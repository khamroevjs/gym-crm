# Gym CRM

This is a Java application developed using the Spring Core framework.

## Prerequisites

Before you can run this application locally, make sure you have the following tools and software installed on your machine:

- [JDK 21](https://www.oracle.com/java/technologies/javase-jdk21-downloads.html)
- [Gradle 8.4](https://gradle.org/install/)
- [Tomcat 10.1.x](https://tomcat.apache.org/)

## Configuration

The application can be run with predefined data. 
You can provide the data by specifying the file paths in the `application.properties` file 
or by defining environment variables.
Here are the environment variables you can set:

`DATA_FILE_TRAINEE` - trainee data file.  
`DATA_FILE_TRAINER` - trainer data file.  
`DATA_FILE_TRAINING` - training data file.  
`DATA_FILE_TRAINING_TYPE` - training type data file.  
`DATA_FILE_USER` - user data file.  